//
//  UIImage+Online.m
//  WeatherForecast
//
//  Created by Evandro Harrison Hoffmann on 2/24/16.
//  Copyright © 2016 Evandro Harrison Hoffmann. All rights reserved.
//

#import "UIImage+Online.h"
#import "AAPIFetcher.h"

@implementation UIImage (Online)

+(NSURLSessionDataTask*)imageWithUrl:(NSString *)url completion:(void (^)(UIImage *image))completion{
    NSURLSessionDataTask* task = [AAPIFetcher fetchDataFromURLString:url completion:^(NSData *data) {
        completion([UIImage imageWithData:data]);
    }];
    
    return task;
}

@end
