//
//  AAPIParser+Instagram.m
//  AwesomePictures
//
//  Created by Evandro Harrison Hoffmann on 3/9/16.
//  Copyright © 2016 Evandro Harrison Hoffmann. All rights reserved.
//

#import "AAPIParser+Instagram.h"

@implementation AAPIParser (Instagram)

#pragma mark - Instagram User

+(InstagramUserObject*)instagramUserWithData:(NSData*)data{
    //json root
    NSDictionary *json = [self jsonWithData:data];
    NSDictionary *dataJson = [json objectForKey:@"data"];
    
    InstagramUserObject *instagramUserObject = [[InstagramUserObject alloc] init];
    instagramUserObject.userID = [self stringValueWithJSONObject:dataJson key:@"id"];
    instagramUserObject.username = [self stringValueWithJSONObject:dataJson key:@"username"];
    instagramUserObject.bio = [self stringValueWithJSONObject:dataJson key:@"bio"];
    instagramUserObject.fullname = [self stringValueWithJSONObject:dataJson key:@"full_name"];
    instagramUserObject.website = [self stringValueWithJSONObject:dataJson key:@"website"];
    instagramUserObject.profilePicture = [self stringValueWithJSONObject:dataJson key:@"profile_picture"];
    
    return instagramUserObject;
}

#pragma mark - Instagram Pictures

+(NSArray*)instagramPicturesWithData:(NSData*)data{
    NSMutableArray *picturesArray = [[NSMutableArray alloc] init];
    
    //json root
    NSDictionary *json = [self jsonWithData:data];
    NSDictionary *dataJson = [json objectForKey:@"data"];
    
    for(NSDictionary *pictureData in dataJson){
        [picturesArray addObject:[self instagramObjectWithJSONObject:pictureData]];
    }
    
    return picturesArray;
}

+(InstagramObject*)instagramObjectWithJSONObject:(NSDictionary*)json{
    InstagramObject *instagramObject = [[InstagramObject alloc] init];
    instagramObject.objectID = [self stringValueWithJSONObject:json key:@"id"];
    
    NSDictionary *imagesJson = [json valueForKey:@"images"];
    
    NSDictionary *imageLowQualityJson = [imagesJson valueForKey:@"low_resolution"];
    instagramObject.imageLowQuality = [self stringValueWithJSONObject:imageLowQualityJson key:@"url"];
    
    NSDictionary *imageThumbnailJson = [imagesJson valueForKey:@"thumbnail"];
    instagramObject.imageThumbnail = [self stringValueWithJSONObject:imageThumbnailJson key:@"url"];
    
    NSDictionary *imageStandardJson = [imagesJson valueForKey:@"standard_resolution"];
    instagramObject.imageStandard = [self stringValueWithJSONObject:imageStandardJson key:@"url"];
    
    return instagramObject;
}

@end
