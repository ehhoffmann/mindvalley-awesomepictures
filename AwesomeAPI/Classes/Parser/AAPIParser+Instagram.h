//
//  AAPIParser+Instagram.h
//  AwesomePictures
//
//  Created by Evandro Harrison Hoffmann on 3/9/16.
//  Copyright © 2016 Evandro Harrison Hoffmann. All rights reserved.
//

#import "AAPIParser.h"
#import "InstagramObject.h"
#import "InstagramUserObject.h"

@interface AAPIParser (Instagram)

+(InstagramUserObject*)instagramUserWithData:(NSData*)data;
+(NSArray*)instagramPicturesWithData:(NSData*)data;
+(InstagramObject*)instagramObjectWithJSONObject:(NSDictionary*)json;

@end
