//
//  InstagramUserObject.h
//  AwesomePictures
//
//  Created by Evandro Harrison Hoffmann on 3/10/16.
//  Copyright © 2016 Evandro Harrison Hoffmann. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InstagramUserObject : NSObject

@property (nonatomic, strong) NSString* username;
@property (nonatomic, strong) NSString* fullname;
@property (nonatomic, strong) NSString* userID;
@property (nonatomic, strong) NSString* bio;
@property (nonatomic, strong) NSString* website;
@property (nonatomic, strong) NSString* profilePicture;

@end
