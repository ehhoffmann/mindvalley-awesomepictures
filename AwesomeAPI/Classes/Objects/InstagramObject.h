//
//  InstagramObject.h
//  AwesomePictures
//
//  Created by Evandro Harrison Hoffmann on 3/9/16.
//  Copyright © 2016 Evandro Harrison Hoffmann. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface InstagramObject : NSObject

@property (nonatomic, strong) NSString *imageLowQuality;
@property (nonatomic, strong) NSString *imageThumbnail;
@property (nonatomic, strong) NSString *imageStandard;
@property (nonatomic, strong) NSString *objectID;

@end
