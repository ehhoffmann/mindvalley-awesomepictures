//
//  AppApiTests.m
//  AwesomePictures
//
//  Created by Evandro Harrison Hoffmann on 3/10/16.
//  Copyright © 2016 Evandro Harrison Hoffmann. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "AAPIParser+Instagram.h"

@interface AAPIParserTests : XCTestCase

@end

@implementation AAPIParserTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testInstagramPictureArrayCorrectAmount{
    NSArray *pictureArray;
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"PinterestUserPictures" ofType:@"json"];
    NSString *jsonString = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
    if (jsonString) {
        NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        pictureArray = [AAPIParser instagramPicturesWithData:data];
    }
    
    XCTAssertNotNil(pictureArray);
    XCTAssertTrue(pictureArray.count == 10);
}

- (void)testInstagramSinglePictureData{
    InstagramObject *instagramObject;
    
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"PinterestUserSinglePicture" ofType:@"json"];
    NSString *jsonString = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
    if (jsonString) {
        NSData *data = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        NSDictionary *json = [AAPIParser jsonWithData:data];
        
        instagramObject = [AAPIParser instagramObjectWithJSONObject:json];
    }
    
    XCTAssertNotNil(instagramObject);
    XCTAssertEqualObjects(instagramObject.imageLowQuality, @"https://scontent.cdninstagram.com/t51.2885-15/s320x320/e35/12531102_1518041465162315_1157186697_n.jpg?ig_cache_key=MTE1NjQ4Mzk5NTQ0ODY2Mzk5OA%3D%3D.2");
    XCTAssertEqualObjects(instagramObject.imageThumbnail, @"https://scontent.cdninstagram.com/t51.2885-15/s150x150/e35/12531102_1518041465162315_1157186697_n.jpg?ig_cache_key=MTE1NjQ4Mzk5NTQ0ODY2Mzk5OA%3D%3D.2");
    XCTAssertEqualObjects(instagramObject.imageStandard, @"https://scontent.cdninstagram.com/t51.2885-15/s640x640/sh0.08/e35/12531102_1518041465162315_1157186697_n.jpg?ig_cache_key=MTE1NjQ4Mzk5NTQ0ODY2Mzk5OA%3D%3D.2");
    
}

@end
