Author: Evandro Harrison Hoffmann
March 10th, 2016
AwesomePictures

# Instructions
It is using CocoaPods. Run "pod install" in terminal prior to execute the app.
Open “AwesomePictures.xcworkspace” file to run the project. 

# App Structure and Comments:

## Coredata
I transferred the AppDelegate’s Database connection to DataAccess file, so it would be easy to transfer it to a Framework and share it with a group. I’m using Singleton design pattern for accessing the data.
The CDHelper is my core data interface, with it I can save time and code lines with simple call ups to the class.
In this app, I’m not using any core data objects as persisting data as it wasn’t the case.

## Library
Here goes all the core app functionalities, such as access to online APIs and web services.
The UserDefaults class saves basic configuration, such as temperature and speed units, using the Memento Design Pattern.

### API
I added the class AppAPI for web services call ups, breaking them into Parsers using Categories, so it would be easy to change any specific web service. I’m using here the Decorator Design Pattern with Categories and the Facade Design Pattern with the API access.
The AppAPI is merely being used as a bridge to the AwesomeAPI, where all the data calls are being made.

## Views
This is a standard MVC usage by separating the user interface from the rest of the code. So if we need to change anything within the appearance of the app, it would not require to rebuild the whole thing.

## Localization
I like adding all files regarding localization (including storyboards as they can also be localized), so it is easy to find them in the project.

## Resources
Where all the media used in the project should be located. Currently only with the image assets.

# Approaches
I started the app with some basic animations and messages, so it would be a pleasant introduction to the app. In general it is very simple, I didn’t worry too much about it’s look, just tried to make it functional. 
The App will basically request the Instagram login credentials, and having this done, it will show the user’s image library, with the option to expand them.
The data is being cached, so that even if the user is offline, the images previously downloaded can be shown. 

# The AwesomeAPI
It will be the generic API to be used in all data requests, no matter what kind of data it’s bringing. In order to test the api, I’m using JSON and Image data with the Instagram. In this model, it should be easy to extend it’s functionality to XML or any other data type just by adding new categories.
As a library, it should be easy to import to any other app that might need it.

## Fetcher
Class that actually gets the data from the server set as parameter, returning it so it can be further processed.

## Parser
Class that parses the retrieved data, according to it’s type. With the Instagram category, it will process the data as a JSON object and return it’s content in an organised way.

# The Unit Tests
As the app is very simple, I have only added a couple of UnitTests to test the Instagram JSON parsing using a real response from Instagram previously fetched.

# What else could I do
There would be much more I could do in here, starting by making it look nicer by developing my own OAuthViewController to instagram and make it look similar to the rest of the app. I could also add other picture APIs and test different sort of data, such as XML or any other…