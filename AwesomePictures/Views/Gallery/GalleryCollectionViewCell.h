//
//  GalleryCollectionViewCell.h
//  AwesomePictures
//
//  Created by Evandro Harrison Hoffmann on 3/9/16.
//  Copyright © 2016 Evandro Harrison Hoffmann. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InstagramObject.h"

@interface GalleryCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *thumbnailImageView;

-(void)configWithInstagramObject:(InstagramObject*)instagramObject;

@end
