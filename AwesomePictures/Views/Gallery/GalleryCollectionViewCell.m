//
//  GalleryCollectionViewCell.m
//  AwesomePictures
//
//  Created by Evandro Harrison Hoffmann on 3/9/16.
//  Copyright © 2016 Evandro Harrison Hoffmann. All rights reserved.
//

#import "GalleryCollectionViewCell.h"
#import "UIView+Loading.h"
#import "UIImage+Online.h"

@implementation GalleryCollectionViewCell

-(void)configWithInstagramObject:(InstagramObject *)instagramObject{
    [self.thumbnailImageView startLoading];
    [UIImage imageWithUrl:instagramObject.imageThumbnail completion:^(UIImage *image) {
        self.thumbnailImageView.image = image;
        [self.thumbnailImageView stopLoading];
    }];
}

@end
