//
//  GalleryCollectionViewController.m
//  AwesomePictures
//
//  Created by Evandro Harrison Hoffmann on 3/9/16.
//  Copyright © 2016 Evandro Harrison Hoffmann. All rights reserved.
//

#import "GalleryCollectionViewController.h"
#import "GalleryCollectionViewCell.h"
#import "AppAPI+Instagram.h"
#import "UIView+Loading.h"
#import "Constants.h"
#import "FullImageViewController.h"
#import "UIImage+Online.h"

@interface GalleryCollectionViewController (){
    BOOL canLoadMore;
    BOOL isRefreshing;
    BOOL isFirstTime;
}
@property (weak, nonatomic) IBOutlet
UIView *backgroundView;

@property (nonatomic, strong) NSMutableArray *instagramPictures;
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@end

@implementation GalleryCollectionViewController

static NSString * const reuseIdentifier = @"thumbnail";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = NSLocalizedString(@"gallery_title", @"");
    
    canLoadMore = true;
    isFirstTime = true;
    
    // Do any additional setup after loading the view.
    self.instagramPictures = [[NSMutableArray alloc] init];
    
    //load background image
    [AppAPI fetchInstagramUserDataWithCallback:^(BOOL success, InstagramUserObject *instagramUser) {
        [UIImage imageWithUrl:instagramUser.profilePicture completion:^(UIImage *image) {
            self.backgroundImageView.image = image;
        }];
    }];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if(isFirstTime){
        isFirstTime = false;
        [self refresh];
    }
}

#pragma mark - Navigation

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if([segue.destinationViewController isKindOfClass:[FullImageViewController class]]){
        UICollectionViewCell *cell = sender;
        
        FullImageViewController *fullImageViewController = segue.destinationViewController;
        fullImageViewController.instagramObject = [self.instagramPictures objectAtIndex:[self.collectionView indexPathForCell:cell].row];
        fullImageViewController.originFrame = CGRectMake(cell.frame.origin.x, cell.frame.origin.y-self.collectionView.contentOffset.y, cell.frame.size.width, cell.frame.size.height);
    }
}

#pragma mark - Events

-(IBAction)unwindToGallery:(UIStoryboardSegue*)sender{
    
}

#pragma mark - Refresh Events

-(void)refresh{
    if(isRefreshing)
        return;
    
    isRefreshing = true;
    [self.view startLoading];
    [AppAPI fetchInstagramUserPicturesWithCallback:^(BOOL success, NSArray *array) {
        isRefreshing = false;
        [self.view stopLoading];
        
        if(array != nil){
            [self.instagramPictures removeAllObjects];
            [self.instagramPictures addObjectsFromArray:array];
            [self.collectionView reloadData];
        }
    }];
}

-(void)nextPage{
    if(isRefreshing)
        return;
    
    isRefreshing = true;
    [self.view startLoading];
    
    InstagramObject *instagramObject = [self.instagramPictures lastObject];
    [AppAPI fetchInstagramUserPicturesWithMaxId:instagramObject.objectID callback:^(BOOL success, NSArray *array) {
        isRefreshing = false;
        [self.view stopLoading];
        
        if(array != nil){
            //if didn't return anything, it means there is nothing else to load
            if(array.count == 0)
                canLoadMore = false;
            else{
                [self.instagramPictures addObjectsFromArray:array];
                [self.collectionView reloadData];
            }
        }
    }];
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.instagramPictures.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    GalleryCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    
    [cell configWithInstagramObject:[self.instagramPictures objectAtIndex:indexPath.row]];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(collectionView.frame.size.width/GALLERY_COLUMNS, collectionView.frame.size.width/GALLERY_COLUMNS);
}

#pragma mark - ScrollView delegate

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (CGRectGetMaxY(scrollView.bounds) == scrollView.contentSize.height) {
        if(canLoadMore){
            [self nextPage];
        }
    }
}

@end
