//
//  FullImageViewController.m
//  AwesomePictures
//
//  Created by Evandro Harrison Hoffmann on 3/10/16.
//  Copyright © 2016 Evandro Harrison Hoffmann. All rights reserved.
//

#import "FullImageViewController.h"
#import "UIImage+Online.h"
#import "UIView+Loading.h"
#import "Constants.h"

@interface FullImageViewController (){
    NSURLSessionDataTask* imageLoadingTask;
}

@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@end

@implementation FullImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.imageView.hidden = true;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    //set the thumbnail image while downloading the big one
    imageLoadingTask = [UIImage imageWithUrl:self.instagramObject.imageThumbnail completion:^(UIImage *image) {
        self.imageView.image = image;
        [self animateIn];
        
        //download the big image
        [self.view startLoadingLockBackground:false];
        imageLoadingTask = [UIImage imageWithUrl:self.instagramObject.imageStandard completion:^(UIImage *image) {
            imageLoadingTask = nil;
            self.imageView.image = image;
            [self.view stopLoading];
            
            if (!image){
                [self dismissViewControllerAnimated:true completion:nil];
            }
        }];
    }];
}

#pragma mark - Events

- (IBAction)closeButtonPressed:(id)sender {
    //cancel download if the case
    if(imageLoadingTask)
        [imageLoadingTask cancel];
    
    [self animateOut];
}

#pragma mark - Animations

-(void)animateIn{
    CGRect originalFrame = self.imageView.frame;
    self.imageView.frame = self.originFrame;
    self.imageView.alpha = 0;
    self.imageView.hidden = false;
    
    [UIView animateWithDuration:FULLIMAGE_TIMEOUT_TIME delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.imageView.frame = originalFrame;
        self.imageView.alpha = 1;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:FULLIMAGE_TIMEOUT_TIME/3 animations:^{
            self.imageView.transform = CGAffineTransformMakeScale(1.1f, 1.1f);
        }completion:^(BOOL finished) {
            [UIView animateWithDuration:FULLIMAGE_TIMEOUT_TIME/3 animations:^{
                self.imageView.transform = CGAffineTransformMakeScale(1, 1);
            }completion:^(BOOL finished) {
                
            }];
        }];
    }];
}

-(void)animateOut{
    [UIView animateWithDuration:FULLIMAGE_TIMEOUT_TIME delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.imageView.frame = self.originFrame;
        self.imageView.alpha = 0;
    } completion:^(BOOL finished) {
        
    }];
}

@end
