//
//  FullImageViewController.h
//  AwesomePictures
//
//  Created by Evandro Harrison Hoffmann on 3/10/16.
//  Copyright © 2016 Evandro Harrison Hoffmann. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InstagramObject.h"

@interface FullImageViewController : UIViewController

@property (nonatomic, assign) CGRect originFrame;
@property (nonatomic, strong) InstagramObject *instagramObject;

@end
