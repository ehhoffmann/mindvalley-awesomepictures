//
//  GreetingViewController.m
//  AwesomePictures
//
//  Created by Evandro Harrison Hoffmann on 3/9/16.
//  Copyright © 2016 Evandro Harrison Hoffmann. All rights reserved.
//

#import "GreetingViewController.h"
#import "NSString+AttributedString.h"
#import "UIColor+AwesomePictures.h"
#import "Constants.h"
#import "UIView+Loading.h"
#import "AppAPI.h"
#import "UserDefaults.h"
#import "InstagramSimpleOAuth.h"

@interface GreetingViewController (){
    int currentMessage;
}

@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (strong, nonatomic) NSArray *messageStack;

@end

@implementation GreetingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //set greeting messages stack
    self.messageStack = [[NSArray alloc] initWithObjects:
                         [NSString attributedStringWith:[[NSArray alloc] initWithObjects:
                                                         [[AttributedText alloc] initWithText:NSLocalizedString(@"greeting_hello", @"") andAttribute:@{NSForegroundColorAttributeName:[UIColor greetingTextColor]}], nil]],
                         [NSString attributedStringWith:[[NSArray alloc] initWithObjects:
                                                         [[AttributedText alloc] initWithText:NSLocalizedString(@"greeting_welcome", @"") andAttribute:@{NSForegroundColorAttributeName:[UIColor greetingTextColor]}],
                                                         [[AttributedText alloc] initWithText:NSLocalizedString(@"greeting_welcome2", @"") andAttribute:@{NSForegroundColorAttributeName:[UIColor greetingTextHighlightColor]}],
                                                         [[AttributedText alloc] initWithText:NSLocalizedString(@"greeting_welcome3", @"") andAttribute:@{NSForegroundColorAttributeName:[UIColor greetingTextColor]}], nil]],
                         [NSString attributedStringWith:[[NSArray alloc] initWithObjects:
                                                         [[AttributedText alloc] initWithText:NSLocalizedString(@"greeting_loginstagram", @"") andAttribute:@{NSForegroundColorAttributeName:[UIColor greetingTextColor]}], nil]],
                         nil];
    
    self.messageLabel.hidden = true;
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if(!UserDefaults.sharedInstance.didRunBefore){
        UserDefaults.sharedInstance.didRunBefore = true;
        [UserDefaults.sharedInstance saveDefaults];
        [self nextMessage];
    }else{
        [self enterApp];
    }
}

#pragma mark - Animations

-(void)fadeInWithMessage:(NSAttributedString*)message{
    self.messageLabel.attributedText = message;
    
    self.messageLabel.alpha = 0;
    self.messageLabel.hidden = false;
    self.messageLabel.center = CGPointMake(self.messageLabel.center.x, self.view.frame.size.height/0.75f);
    
    [UIView animateWithDuration:GREETING_ANIMATION_TIME delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.messageLabel.center = self.view.center;
        self.messageLabel.alpha = 1;
    } completion:^(BOOL finished) {
        float proportionalTimeout = (GREETING_ANIMATION_TIME*self.messageLabel.text.length)/30;
        
        [NSTimer scheduledTimerWithTimeInterval:proportionalTimeout target:self selector:@selector(fadeAway) userInfo:nil repeats:false];
    }];
}

-(void)fadeAway{
    [UIView animateWithDuration:GREETING_ANIMATION_TIME/2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.messageLabel.center = CGPointMake(self.messageLabel.center.x, self.view.frame.size.height/4);
        self.messageLabel.alpha = 0;
    } completion:^(BOOL finished) {
        self.messageLabel.hidden = true;
        [self nextMessage];
    }];
}

-(void)nextMessage{
    if (currentMessage < self.messageStack.count){
        [self fadeInWithMessage:[self.messageStack objectAtIndex:currentMessage]];
        currentMessage++;
    }else{
        [self enterApp];
    }
}

-(void)enterApp{
    //next page
    if(AppAPI.sharedInstance.instagramToken){
        [self.view stopLoading];
        
        NSLog(@"instagram token: %@", AppAPI.sharedInstance.instagramToken);
        
        UINavigationController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"MainNavigation"];
        navigationController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:navigationController animated:true completion:nil];
    }else{
        [self.view startLoading];
        InstagramSimpleOAuthViewController *instagramViewController = [[InstagramSimpleOAuthViewController alloc] initWithClientID:Instagram_client_key clientSecret:Instagram_client_secret callbackURL:Instagram_callback_url completion:^(InstagramLoginResponse *response, NSError *error) {
            AppAPI.sharedInstance.instagramToken = response.accessToken;
        }];
        instagramViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        instagramViewController.permissionScope = Instagram_permissions_scope;
        [self presentViewController:instagramViewController animated:true completion:nil];
    }
}

@end
