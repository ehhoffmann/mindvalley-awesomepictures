//
//  Constants.h
//  AwesomePictures
//
//  Created by Evandro Harrison Hoffmann on 3/9/16.
//  Copyright © 2016 Evandro Harrison Hoffmann. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

//----------------------------
#pragma mark - Services

#define TIMEOUT_TIME 10.0f
#define NEXT_PAGET_OFFSET 20.0f
#define PICTURE_PAGE_COUNT 10
#define CACHE_CAPACITY_IN_MB 10

#define Instagram_client_key @"e9c54bf39b2c495e801c25fa6b4bf947"
#define Instagram_client_secret @"6876e40230b4402c89da510a92b4a454"
#define Instagram_callback_url [NSURL URLWithString:@"http://itsdayoff.com"]
#define Instagram_permissions_scope @[@"basic", @"comments", @"relationships", @"likes", @"public_content"]

//----------------------------
#pragma mark - Gallery

#define GALLERY_COLUMNS 2

//----------------------------
#pragma mark - Animations

#define GREETING_ANIMATION_TIME 0.7f
#define GREETING_TIMEOUT_TIME 1.0f

#define FULLIMAGE_TIMEOUT_TIME 0.3f

#endif /* Constants_h */
