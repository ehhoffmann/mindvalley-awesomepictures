//
//  DesignableView.m
//  OnDemand
//
//  Created by Evandro Harrison Hoffmann on 03/08/16.
//  Copyright © 2016 Evandro Harrison Hoffmann. All rights reserved.
//

#import "DesignableView.h"

@implementation DesignableView

-(void)setCornerRadius:(CGFloat)cornerRadius{
    self.layer.cornerRadius = cornerRadius;
}

@end
