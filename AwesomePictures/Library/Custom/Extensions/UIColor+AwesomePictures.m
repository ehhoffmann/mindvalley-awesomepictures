//
//  UIColor+UIColor_AwesomePictures.m
//  AwesomePictures
//
//  Created by Evandro Harrison Hoffmann on 2/22/16.
//  Copyright © 2016 Evandro Harrison Hoffmann. All rights reserved.
//

#import "UIColor+AwesomePictures.h"

@implementation UIColor (AwesomePictures)

#define RGBA(r,g,b,a) [UIColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:a]

+(UIColor *)greyBackgroundColor{
    return [UIColor colorWithRed:0.91 green:0.91 blue:0.91 alpha:1];
}

+(UIColor *)greetingTextColor{
    return RGBA(255, 255, 255, 0.8);
}

+(UIColor *)greetingTextHighlightColor{
    return RGBA(255, 255, 255, 1);
}

@end
