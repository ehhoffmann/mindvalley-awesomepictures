//
//  UIColor+UIColor_AwesomePictures.h
//  AwesomePictures
//
//  Created by Evandro Harrison Hoffmann on 2/22/16.
//  Copyright © 2016 Evandro Harrison Hoffmann. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (AwesomePictures)

+ (UIColor*) greyBackgroundColor;
+ (UIColor*) greetingTextColor;
+ (UIColor *)greetingTextHighlightColor;

@end
