//
//  UIView+Loading.m
//  AwesomePictures
//
//  Created by Evandro Harrison Hoffmann on 3/9/16.
//  Copyright © 2016 Evandro Harrison Hoffmann. All rights reserved.
//

#import "UIView+Loading.h"
#import "LoadingView.h"

@implementation UIView (Loading)

-(void)startLoading{
    LoadingView *loadingView = [[LoadingView alloc] initWithFrame:self.frame backgroundLocked:true];
    [self addSubview:loadingView];
    [loadingView startAnimating];
}

-(void)startLoadingLockBackground:(BOOL)backgroundLocked{
    LoadingView *loadingView = [[LoadingView alloc] initWithFrame:self.frame backgroundLocked:backgroundLocked];
    [self addSubview:loadingView];
    [loadingView startAnimating];
}

-(void)stopLoading{
    for(int i = (int)self.subviews.count-1; i >= 0; i-- ){
        if ([[self.subviews objectAtIndex:i] isKindOfClass:[LoadingView class]]){
            [[self.subviews objectAtIndex:i] stopAnimating];
            break;
        }
    }
}

@end
