//
//  LoadingView.m
//  AwesomePictures
//
//  Created by Evandro Harrison Hoffmann on 3/9/16.
//  Copyright © 2016 Evandro Harrison Hoffmann. All rights reserved.
//

#import "LoadingView.h"

@implementation LoadingView{
    BOOL animating;
}

-(instancetype)initWithFrame:(CGRect)frame backgroundLocked:(BOOL)backgroundLocked{
    self = [super initWithFrame:frame];
    if(self){
        NSArray *xibViews = [[NSBundle mainBundle] loadNibNamed:@"LoadingView" owner:self options:nil];
        [self addSubview:[xibViews objectAtIndex:0]];
        self.backgroundColor = [UIColor clearColor];
        
        //disables backgroundlock
        if(!backgroundLocked)
            self.frame = CGRectMake(frame.size.width/2-self.mainView.frame.size.width/2, frame.size.height/2-self.mainView.frame.size.height/2, self.mainView.frame.size.width, self.mainView.frame.size.height);
        self.mainView.center = CGPointMake(self.frame.size.width/2, self.frame.size.height/2);
        self.mainView.backgroundColor = [UIColor clearColor];
    }
    return self;
}

#pragma mark - Animations

-(void)startAnimating{
    self.mainView.alpha = 0;
    self.mainView.transform = CGAffineTransformMakeScale(0.5f, 0.5f);
    
    [UIView animateWithDuration:0.5f delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.mainView.alpha = 1;
        self.mainView.transform = CGAffineTransformMakeScale(1, 1);
    } completion:^(BOOL finished) {
        
    }];
    
    if (!animating) {
        animating = YES;
        [self makeSpinAnimationWithOptions:UIViewAnimationOptionCurveEaseIn];
    }
}

-(void)stopAnimating{
    animating = NO;
    
    [UIView animateWithDuration:0.3f delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.mainView.alpha = 0;
        self.mainView.transform = CGAffineTransformMakeScale(1.3f, 1.3f);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

-(void)makeSpinAnimationWithOptions:(UIViewAnimationOptions)options{
    [UIView animateWithDuration:0.3f delay:0 options:options animations:^{
        self.mainView.transform = CGAffineTransformRotate(self.mainView.transform, M_PI_2 / 2);
    } completion:^(BOOL finished) {
        if (finished) {
            if (animating) {
                // if flag still set, keep spinning with constant speed
                [self makeSpinAnimationWithOptions: UIViewAnimationOptionCurveLinear];
            } else if (options != UIViewAnimationOptionCurveEaseOut) {
                // one last spin, with deceleration
                [self makeSpinAnimationWithOptions: UIViewAnimationOptionCurveEaseOut];
            }
        }
    }];
}

@end
