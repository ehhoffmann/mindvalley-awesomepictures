//
//  LoadingView.h
//  AwesomePictures
//
//  Created by Evandro Harrison Hoffmann on 3/9/16.
//  Copyright © 2016 Evandro Harrison Hoffmann. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DesignableView.h"

@interface LoadingView : UIView

@property (strong, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet DesignableView *ball1View;
@property (weak, nonatomic) IBOutlet DesignableView *ball2View;
@property (weak, nonatomic) IBOutlet DesignableView *ball3View;

-(instancetype)initWithFrame:(CGRect)frame backgroundLocked:(BOOL)backgroundLocked;
-(void)startAnimating;
-(void)stopAnimating;

@end
