//
//  AppAPI+Instagram.h
//  AwesomePictures
//
//  Created by Evandro Harrison Hoffmann on 3/9/16.
//  Copyright © 2016 Evandro Harrison Hoffmann. All rights reserved.
//

#import "AppAPI.h"

@interface AppAPI (Instagram)

//information
+(void)fetchInstagramUserDataWithCallback:(void (^)(BOOL success, InstagramUserObject *instagramUser))completion;

//media
+(void)fetchInstagramUserPicturesWithCallback:(void (^)(BOOL success, NSArray *array))completion;
+(void)fetchInstagramUserPicturesWithMaxId:(NSString*)minId callback:(void (^)(BOOL success, NSArray *array))completion;

@end
