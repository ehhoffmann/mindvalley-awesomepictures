//
//  AppLibrary.h
//  WeatherForecast
//
//  Created by Evandro Harrison Hoffmann on 2/23/16.
//  Copyright © 2016 Evandro Harrison Hoffmann. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "InstagramUserObject.h"

@interface AppAPI: NSObject

+(AppAPI*)sharedInstance;

@property (nonatomic, strong) NSString *instagramToken;
@property (nonatomic, strong) InstagramUserObject *instagramUser;

@end
