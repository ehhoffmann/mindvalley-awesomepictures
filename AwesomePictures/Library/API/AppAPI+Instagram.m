//
//  AppAPI+Instagram.m
//  AwesomePictures
//
//  Created by Evandro Harrison Hoffmann on 3/9/16.
//  Copyright © 2016 Evandro Harrison Hoffmann. All rights reserved.
//

#import "AppAPI+Instagram.h"
#import "AAPIFetcher.h"
#import "AAPIParser+Instagram.h"
#import "Constants.h"

#define InstagramMainUrl @"https://api.instagram.com/v1/"
#define InstagramSelfInfo @"users/self/"
#define InstagramSelfPictures @"users/self/media/recent/"
#define InstagramUserPictures(username) [NSString stringWithFormat:@"users/%@/media/recent/", username]

@implementation AppAPI (Instagram)

+(void)fetchInstagramUserDataWithCallback:(void (^)(BOOL success, InstagramUserObject *instagramUser))completion{
    NSString *urlString = [NSString stringWithFormat:@"%@%@?access_token=%@&count=%d", InstagramMainUrl, InstagramSelfInfo, self.sharedInstance.instagramToken, PICTURE_PAGE_COUNT];
    
    [AAPIFetcher fetchDataFromURLString:urlString timeout:TIMEOUT_TIME completion:^(BOOL success, NSData *data) {
        if(success){
            AppAPI.sharedInstance.instagramUser = [AAPIParser instagramUserWithData:data];
            completion(true, AppAPI.sharedInstance.instagramUser);
        }else{
            completion(false, nil);
        }
    }];
}

+(void)fetchInstagramUserPicturesWithCallback:(void (^)(BOOL success, NSArray *pictures))completion{
    NSString *urlString = [NSString stringWithFormat:@"%@%@?access_token=%@&count=%d", InstagramMainUrl, InstagramSelfPictures, self.sharedInstance.instagramToken, PICTURE_PAGE_COUNT];
    
    [AAPIFetcher fetchDataFromURLString:urlString timeout:TIMEOUT_TIME completion:^(BOOL success, NSData *data) {
        if(success){
            completion(true, [AAPIParser instagramPicturesWithData:data]);
        }else{
            completion(false, nil);
        }
    }];
}

+(void)fetchInstagramUserPicturesWithMaxId:(NSString *)maxId callback:(void (^)(BOOL, NSArray *))completion{
    NSString *urlString = [NSString stringWithFormat:@"%@%@?access_token=%@&max_id=%@", InstagramMainUrl, InstagramSelfPictures, self.sharedInstance.instagramToken, maxId];
    
    [AAPIFetcher fetchDataFromURLString:urlString timeout:TIMEOUT_TIME completion:^(BOOL success, NSData *data) {
        if(success){
            completion(true, [AAPIParser instagramPicturesWithData:data]);
        }else{
            completion(false, nil);
        }
    }];
}



@end
