//
//  AppLibrary.m
//  WeatherForecast
//
//  Created by Evandro Harrison Hoffmann on 2/23/16.
//  Copyright © 2016 Evandro Harrison Hoffmann. All rights reserved.
//

#import "AppAPI.h"

@implementation AppAPI

+(AppAPI *)sharedInstance{
    static AppAPI *appApi;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        appApi = [[AppAPI alloc] init];
    });
    return appApi;
}

@end
