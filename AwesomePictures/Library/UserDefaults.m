//
//  UserDefaults.m
//  AwesomePictures
//
//  Created by Evandro Harrison Hoffmann on 3/9/16.
//  Copyright © 2016 Evandro Harrison Hoffmann. All rights reserved.
//

#import "UserDefaults.h"

@implementation UserDefaults

#define didRunBeforeKey @"didRunBeforeRun"

+(UserDefaults *)sharedInstance{
    static UserDefaults *userDefaults;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        userDefaults = [[UserDefaults alloc] init];
        [userDefaults loadDefaults];
    });
    return userDefaults;
}

-(void)saveDefaults{
    [[NSUserDefaults standardUserDefaults] setBool:self.didRunBefore forKey:didRunBeforeKey];
}

-(void)loadDefaults{
    self.didRunBefore = [[NSUserDefaults standardUserDefaults] boolForKey:didRunBeforeKey];
}

@end
