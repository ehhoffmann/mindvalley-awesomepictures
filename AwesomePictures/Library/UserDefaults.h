//
//  UserDefaults.h
//  AwesomePictures
//
//  Created by Evandro Harrison Hoffmann on 3/9/16.
//  Copyright © 2016 Evandro Harrison Hoffmann. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Constants.h"

@interface UserDefaults : NSObject

@property (nonatomic, assign) BOOL didRunBefore;

+ (UserDefaults*)sharedInstance;

-(void)saveDefaults;
-(void)loadDefaults;

@end
